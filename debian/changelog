csoundqt (1.1.3+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.3+dfsg

 -- Dennis Braun <snd@debian.org>  Sun, 10 Nov 2024 18:44:02 +0100

csoundqt (1.1.2+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.2+dfsg
  * Omit the dfsg count
  * Bump Standards-Version to 4.7.0
  * Use the default ci config from salsa

 -- Dennis Braun <snd@debian.org>  Mon, 02 Sep 2024 21:33:25 +0200

csoundqt (1.1.1+dfsg0-1) unstable; urgency=medium

  * New upstream version 1.1.1+dfsg0
  * Build with rtmidi support
  * Drop desktop patch, applied by upstream
  * d/copyright: Remove Bach-Invention_1.csd and CC license
  * Drop obsolete d/csoundqt-examples.lintian-overrides
  * Add salsa ci config
  * Change my email address to snd@debian.org

 -- Dennis Braun <snd@debian.org>  Mon, 28 Nov 2022 21:22:42 +0100

csoundqt (1.1.0+dfsg0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libcsnd-dev and
      libcsound64-dev.
  * debian/copyright: use spaces rather than tabs to start continuation lines.

  [ Dennis Braun ]
  * New upstream version 1.1.0+dfsg0
  * Remove shebang and obsolete en-US entries from desktop file
  * d/control:
    + Bump Standards-Version to 4.6.1
    + Set RRR: no
  * d/copyright:
    + Fix path to Sruti-Drone_Box.csd
    + Add bin/win-installer.iss to Files-Excluded
    + Add Circle_Map.csd entry and add Tarmo Johannes to copyright holders
  * Drop all patches, applied by upstream
  * Drop d/csoundqt.lintian-overrides
  * d/watch: Use +dfsg instead of ~dfsg

 -- Dennis Braun <d_braun@kabelmail.de>  Sat, 18 Jun 2022 19:20:22 +0200

csoundqt (0.9.8.1~dfsg0-1) unstable; urgency=medium

  * New upstream version 0.9.8.1~dfsg0
    + Exclude non-free Sruti-Drone_Box.csd
  * Add more infos to the desktop file
  * Fix spell errors
  * Add me as uploader
  * Bump dh-compat to 13
    + d/rules: Remove obsolete 'dh_missing --fail-missing'
  * Bump S-V to 4.5.1
  * Rewrite d/copyright in DEP5 format
  * Mark csoundqt-examples as M-A: foreign
  * Add d/csoundqt-examples.lintian-overrides
  * d/rules: Add hardening
  * Bump d/watch version to 4

 -- Dennis Braun <d_braun@kabelmail.de>  Sun, 07 Feb 2021 14:51:34 +0100

csoundqt (0.9.7-2) unstable; urgency=medium

  [ Felipe Sateler ]
  * Remove local-options.
    It is bad practice to have different dpkg options for maintainer than anyone else
  * Bump Standards-Version
  * Ignore lintian warning about the desktop file being a script
  * Add manpage alias for CsoundQt-d-cs6

  [ Helmut Grohne ]
  * Fix FTBFS: Search for a multiarch csound.
    Csound was recently moved into a multiarch directory. Therefore, we need
    to hint csoundqt to find it. (Closes: #959520)

 -- Felipe Sateler <fsateler@debian.org>  Sat, 09 May 2020 11:38:00 -0400

csoundqt (0.9.7-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * Use debhelper-compat instead of debian/compat

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org
  * New upstream version 0.9.7
  * Bump debhelper from old 10 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Drop unnecessary dh arguments: --parallel, --parallel
  * Drop all patches, applied upstream
  * Adjust examples, templates and docs for new version

 -- Felipe Sateler <fsateler@debian.org>  Thu, 06 Feb 2020 00:49:31 -0300

csoundqt (0.9.4-1) unstable; urgency=medium

  * New upstream version 0.9.4
  * Fix csoundqt binary softlink to point to correct binary
  * Use dh_missing instead of --fail-missing flag to dh_install
  * Bump debhelper compat level to 10
  * Bump standards version (no changes needed)
  * Don't do duplicate dh_install work

 -- Felipe Sateler <fsateler@debian.org>  Tue, 27 Jun 2017 22:04:14 -0400

csoundqt (0.9.3-1) unstable; urgency=medium

  * New upstream version
  * Remove qmake path parameters.
    The build script is now able to autodetect all this
  * Enable recording support

 -- Felipe Sateler <fsateler@debian.org>  Thu, 01 Dec 2016 19:54:01 -0300

csoundqt (0.9.2.2-2) unstable; urgency=medium

  * Fix binary-only builds

 -- Felipe Sateler <fsateler@debian.org>  Fri, 16 Sep 2016 22:25:38 -0300

csoundqt (0.9.2.2-1) unstable; urgency=medium

  * New upstream release
    - Drop patches, applied upstream
  * Bump debhelper to compat 9
  * Switch from cdbs to dh
  * Bump standards version

 -- Felipe Sateler <fsateler@debian.org>  Mon, 12 Sep 2016 15:02:23 -0300

csoundqt (0.9.2.1-3) unstable; urgency=medium

  * Resolve ambiguous conversion from uint64_t to QVariant.
    Closes: #830420

 -- Felipe Sateler <fsateler@debian.org>  Sun, 10 Jul 2016 22:40:27 -0400

csoundqt (0.9.2.1-2) unstable; urgency=medium

  * Split examples into separate Arch: all package.
    Closes: #821825

 -- Felipe Sateler <fsateler@debian.org>  Mon, 09 May 2016 10:12:57 -0300

csoundqt (0.9.2.1-1) unstable; urgency=medium

  * Imported Upstream version 0.9.2.1
  * Use canonical Vcs-* urls
  * Drop install-path.patch, applied upstream
  * Remove execute bit from qutecsound shared files

 -- Felipe Sateler <fsateler@debian.org>  Wed, 13 Apr 2016 20:22:07 -0300

csoundqt (0.9.2-1) unstable; urgency=medium

  * New upstream release
    - Drop qhash-type.patch, applied upstream
    - Use upstream install target, add patch to make it work
  * Drop menu entry, as now we have desktop entry
  * Bump Standards Version
  * Update upstream urls to use CsoundQt github organization

 -- Felipe Sateler <fsateler@debian.org>  Mon, 07 Mar 2016 22:23:57 -0300

csoundqt (0.9.1-1) unstable; urgency=medium

  * New upstream version
  * debian/patches/qhash-type.patch: Add missing QHash include.
    Closes: #803757

 -- Felipe Sateler <fsateler@debian.org>  Fri, 06 Nov 2015 21:31:19 -0300

csoundqt (0.9.0-1) unstable; urgency=medium

  * Imported Upstream version 0.9.0
    - Bump minimum required version of csound to 6.05
    - Drops dependency on libsndfile1-dev
  * Use Qt5 for building
  * Bump standards version (no changes needed)
  * Drop transitional qutecsound package

 -- Felipe Sateler <fsateler@debian.org>  Sat, 02 May 2015 23:28:04 -0300

csoundqt (0.8.3-1) unstable; urgency=medium

  * New upstream release
    - Drop no-sf-graphic.patch, included upstream
  * Use the new csound debugging apis. Requires csound >= 6.03

 -- Felipe Sateler <fsateler@debian.org>  Tue, 10 Jun 2014 17:16:13 -0400

csoundqt (0.8.2~dfsg-1) unstable; urgency=medium

  * New upstream release
    - Repackage tarball to exclude non-free icon (Closes: #735499)
  * Bump standards version

 -- Felipe Sateler <fsateler@debian.org>  Fri, 31 Jan 2014 12:05:02 -0300

csoundqt (0.8.0-3) unstable; urgency=low

  * Link with csnd6 instead of csnd, upstream renamed the library.
    Tighten build-dep accordingly. Prevents FTBFS.

 -- Felipe Sateler <fsateler@debian.org>  Fri, 22 Nov 2013 15:16:09 -0300

csoundqt (0.8.0-2) unstable; urgency=low

  * Make csoundqt Break+Replace qutecsound. Closes: #722557

 -- Felipe Sateler <fsateler@debian.org>  Thu, 12 Sep 2013 17:35:29 -0300

csoundqt (0.8.0-1) unstable; urgency=low

  * New upstream release, renamed to csoundqt
  * Remove patch, applied upstream
  * Add patch to not hardcode paths
  * Update Vcs-* fields
  * Build-dep on csound6

 -- Felipe Sateler <fsateler@debian.org>  Wed, 14 Aug 2013 19:28:08 -0400

qutecsound (0.6.1-2) unstable; urgency=low

  * Add patch to enable configuration of search paths for libraries
  * Add multiarch dir to libsndfile search path. Closes: #642735

 -- Felipe Sateler <fsateler@debian.org>  Sun, 25 Sep 2011 22:54:33 -0300

qutecsound (0.6.1-1) unstable; urgency=low

  [ Felipe Sateler ]
  * New upstream release
   - Drop patch 2000-default-html-dir: adopted upstream
  * Set the Debian Multimedia team as maintainer, myself as uploader
  * Add Vcs-* Fields
  * Switch to 3.0 source format
  * Update copyright file
    - Update years
    - Add notice about pyroom.svg
    - Add examples authors

  [ Alessio Treglia ]
  * Add gbp config file.
  * Update Standards.
  * Add myself to the Uploaders field.
  * No patches to apply, remove debian/patches/.
  * debian/watch: Clean-up.
  * Unapply patches after gbp'ing, abort on upstream changes.
  * Update debian/qutecsound.docs to install the new copy of the release notes.

 -- Felipe Sateler <fsateler@debian.org>  Sun, 03 Apr 2011 10:54:03 -0400

qutecsound (0.6.0-1) unstable; urgency=low

  * Fix watch file for -beta releases.
  * New upstream release.

 -- Felipe Sateler <fsateler@debian.org>  Thu, 29 Jul 2010 18:24:14 -0400

qutecsound (0.6.0~beta-1) experimental; urgency=low

  * New upstream release
  * Tell qmake which project file we want
  * Upstream now creates qutecsound with a precision suffix, remove it
  * Install release notes
  * Drop DMUA field, not needed anymore
  * Update my email address
  * Bump standards-version (no changes needed)

 -- Felipe Sateler <fsateler@debian.org>  Tue, 06 Jul 2010 00:52:24 -0400

qutecsound (0.5.0-1) unstable; urgency=low

  * New Upstream version
    - Refresh patches
  * Use cdbs support for parallel builds
  * Bump Standards-Version (no changes needed)
  * Explicitly use 1.0 source format
  * Set DM-Upload-Allowed flag

 -- Felipe Sateler <fsateler@gmail.com>  Wed, 14 Apr 2010 18:28:45 -0400

qutecsound (0.4.3-1) unstable; urgency=low

  * New upstream release.
  * Update patches
  * Use upstream provided xpm image
  * Document use of quilt in README.source

 -- Felipe Sateler <fsateler@gmail.com>  Sun, 01 Nov 2009 23:27:42 -0300

qutecsound (0.4.1-1) unstable; urgency=low

  * Initial release (Closes: #511631)
  * Add menu entry
  * Add a man page for qutecsound
  * Set the default location for the manual in Debian.
  * Add patch to fix FTBFS with gcc 4.4 from upstream svn

 -- Felipe Sateler <fsateler@gmail.com>  Sun, 14 Jun 2009 13:27:49 +1000
